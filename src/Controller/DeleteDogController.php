<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DeleteDogController extends Controller
{
    /**
     * @Route("/delete/dog", name="delete_dog")
     */
    public function index()
    {
        return $this->render('delete_dog/index.html.twig', [
            'controller_name' => 'DeleteDogController',
        ]);
    }
}
