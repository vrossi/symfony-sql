<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\DogRepository;
use App\Entity\SmallDog;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(DogRepository $repo) {
        // $nouveauChien = new SmallDog();
        // $nouveauChien->name = "name test php";
        // $nouveauChien->breed = "race test php";
        // $nouveauChien->age = "age test php";
        
        // $repo->add($nouveauChien);
        // dump($nouveauChien);

        $return = $repo->getAll();

        return $this->render('home/index.html.twig', [
            'returnGetAll' => $return
        ]);
    }
}



// 2. Toujours dans la route index, utiliser le DogRepository (qui est en argument de la méthode du controller) pour faire un getAll() et stocker le retour du getAll dans une variable