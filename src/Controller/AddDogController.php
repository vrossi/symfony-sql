<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\DogRepository;
use App\Entity\SmallDog;
use Symfony\Component\HttpFoundation\Request;
use App\Form\SmallDogType;

class AddDogController extends Controller
{
    /**
     * @Route("/add/dog", name="add_dog")
     */
    public function index(DogRepository $repo, Request $request) {
       

        $form = $this->createForm(SmallDogType::class);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $task = $repo->add($form->getData());

                return $this->redirectToRoute("home");
            }      

        return $this->render('add_dog/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}

