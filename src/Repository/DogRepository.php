<?php

namespace App\Repository;

use App\Entity\SmallDog;


class DogRepository
{

  public function getAll() : array {
$dogs = [];
    try {

      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $query = $cnx->prepare("SELECT * FROM small_dog");
        $query->execute();

        foreach ($query->fetchAll() as $row) {
         $dog = new SmallDog();
         $dog->fromSQL($row);
         $dogs[] = $dog;
        }

    } catch (\PDOException $e) {
      dump($e);
    }
  
    return $dogs;
  }

public function add(SmallDog $dog){
  try {

    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
    $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); 

    $query = $cnx->prepare("INSERT INTO small_dog (name, breed, age) VALUES (:name, :breed, :age)");
    $query->bindValue(":name", $dog->name);
    $query->bindValue(":breed", $dog->breed);
    $query->bindValue(":age", $dog->age);
    
    $query->execute();

    $dog->id = intval($cnx->lastInsertId());

  } catch (\PDOException $e) {
    dump($e);
  }
}


public function delete(int $id){      
  try {
    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
    $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); 

    $query = $cnx->prepare("DELETE FROM small_dog WHERE id=:id");
   
    $query->bindValue(":id", $id);
    
    return $query->execute();

  } catch (\PDOException $e) {
    dump($e);
  }
  
}

public function update(SmallDog $dog) {
  try {

    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
    $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); 

    $query = $cnx->prepare(" UPDATE small_dog SET name=:name, breed=:breed, age=:age WHERE id=:id");
    $query->bindValue(":name", $dog->name);
    $query->bindValue(":breed", $dog->breed);
    $query->bindValue(":age", $dog->age);
    $query->bindValue(":id", $dog->id);
    
    
    $query->execute();

  } catch (\PDOException $e) {
    dump($e);
  }
  return false;
}


public function getById(int $id) {
  try {

    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
    $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); 

    $query = $cnx->prepare(" SELECT * FROM small_dog  WHERE id=:id");
    $query->bindValue(":id", $id);
    
    $query->execute();

    $result = $query->fetchAll();// il renvoi le tableau contenant tt les lignes de resultats

    if (count($result)=== 1) {
      $dog = new SmallDog();
      $dog->fromSQL($result[0]);
      return $dog;
    }

  } catch (\PDOException $e) {
    dump($e);
  }
  return null;
}

}
