<?php 


namespace App\Entity;

class SmallDog{
public $id;
public $name;
public $breed;
public $age;

public function fromSQL(array $sql) {

 $this->id =$sql["id"];
 $this->name =$sql["name"];
 $this->breed =$sql["breed"];
 $this->age =$sql["age"];
 
}


}